// Load HTTP module
const http = require("http");
const express = require('express');
const spotifyApi = require('./spotify-api');
const morgan = require('morgan');
const cors = require('cors')
const hostname = "127.0.0.1";
const port = 8000;

// Create HTTP server 
const app = express();
app.use(cors())
app.use(morgan('combined'));

app.get('/tracks/search', async function (req, res) {
    try {
        const text = req.query.text
        const response = await spotifyApi.search(text);
        if (response.status === 200) {
            const data = await response.json();
            res.send(200, data);
        } else {
            console.log(res); 
            throw new Error("unexpeted response code")
        }
    }
    catch (ex) {
        console.log('error', ex)
        // console.log('error', ex)
        res.send(500);
    }
});

app.get('/audio-features', async function (req, res) {
    try {
        const id = req.query.id
        const response = await spotifyApi.audioFeatures(id);
        if (response.status === 200) {
            const data = await response.json();
            res.send(200, data);
        } else {
            console.log(res); 
            throw new Error("unexpeted response code")
        }
    }
    catch (ex) {
        console.log('error', ex)
        // console.log('error', ex)
        res.send(500);
    }
});


app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});